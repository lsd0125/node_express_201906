CREATE TABLE IF NOT EXISTS `sales` (
`sid` int(11) NOT NULL,
`sales_id` varchar(255) NOT NULL,
`name` varchar(255) NOT NULL,
`birthday` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
INSERT INTO `sales` (`sid`, `sales_id`, `name`, `birthday`) VALUES
(1, 'A001', '李小明', '1990-09-13'),
(2, 'B002', '陳小華', '1989-01-10'),
(3, 'A002', '吳小安', '1990-02-20');
ALTER TABLE `sales`
ADD PRIMARY KEY (`sid`),
ADD UNIQUE KEY `sales_id` (`sales_id`);
ALTER TABLE `sales`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

CREATE TABLE IF NOT EXISTS `users` (
`sid` int(11) NOT NULL,
`account` varchar(255) NOT NULL,
`password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

ALTER TABLE `users`
ADD PRIMARY KEY (`sid`),
ADD UNIQUE KEY `account` (`account`);
ALTER TABLE `users`
MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

INSERT INTO `users` (`account`, `password`) VALUES
('john', SHA1('1234')),
('bill', SHA1('1234'))