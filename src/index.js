const express = require('express');
const fs = require('fs');
const url = require('url');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({dest: 'tmp_uploads/'});
const session = require('express-session');
const moment = require('moment-timezone');
const mysql = require('mysql');
const bluebird = require('bluebird');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'test'
});

db.connect();
bluebird.promisifyAll(db);

const app = express();

app.set('view engine', 'ejs');

app.use(express.static(__dirname + '/../public'));
app.use(bodyParser.urlencoded({extended:false}));
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'sdfgdsfgdsfg',
    cookie: {
        maxAge: 1200000
    }
}));


app.use('/', (req, res, next)=>{

    console.log('111: ' + req.url);

    if(req.session.loginUser){
        res.locals.isLogined = !! req.session.loginUser;
        res.locals.loginUser = req.session.loginUser;
    }

    next();
});

app.get('/', (req, res)=>{
    res.render('home', {
        name: 'Shinder'
    });
});

app.get('/sales', (req, res)=>{
    //res.send('<h2>hi abc</h2>');
    const data = require(__dirname + '/../data/sales');

    res.render('sales', {sales: data});
});

app.get('/try-querystring',(req, res)=>{
    const urlParts = url.parse(req.url, true);
    console.log(urlParts);
    res.json(urlParts);
});


app.post('/try-post', (req, res)=>{
    const result = {
        success: true,
        body: req.body,
        data: req.body.a*req.body.b
    };
    res.json(result);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});
app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', req.body);
});

app.get('/try-upload', (req, res)=>{
    res.render('try-upload');
});
app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    // console.log(req.file);
    if(req.file && req.file.originalname){
        if(/\.(jpg|jpeg|png)$/i.test(req.file.originalname)){

            fs.createReadStream(req.file.path)
                .pipe(
                    fs.createWriteStream('./public/img/' + req.file.originalname)
                );
            res.render('try-upload', {
                result: true,
                name: req.body.name,
                avatar: '/img/' + req.file.originalname
            });
            return;
        }
    }

    res.render('try-upload', {
        result: false,
        name: '',
        avatar: ''
    });

});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});

app.get(/^\/09\d{2}\-?\d{3}\-?\d{3}$/, (req, res)=>{
    let str = req.url.slice(1);

    str = str.split('-').join('');

    res.send('手機: '+str);
});

let admin1 = require(__dirname + '/admins/admin1');
admin1(app);

let admin2Router = require(__dirname + '/admins/admin2');
app.use(admin2Router);

let admin3Router = require(__dirname + '/admins/admin3');
app.use('/admin3', admin3Router);


app.get('/try-session', (req, res)=>{
    req.session.views = req.session.views || 0;
    req.session.views++;
    res.contentType('text/plain');
    res.write('views: ' + req.session.views + "\n");

    res.end(JSON.stringify(req.session));
});

app.get('/login', (req, res)=>{
    const data = {};
    if(req.session.flashMsg){
        data.flashMsg = req.session.flashMsg;
        delete req.session.flashMsg;
    }

    //data.isLogined = !! req.session.loginUser;
    //data.loginUser = req.session.loginUser;
    data.oriUser = req.session.oriUser || '';
    data.oriPass = req.session.oriPass || '';
    res.locals.hello = 'Hello World';
    res.render('login', data);
});
app.post('/login', (req, res)=>{
    //req.body.user
    //req.body.password
    let sql = "SELECT * FROM `users` WHERE `account`=? AND `password`=SHA1(?) ";
    db.query(sql, [
        req.body.user,
        req.body.password
    ], (error, results)=>{
        console.log('201:', results);
        if(results[0]){
            console.log('202:', results[0]);
            console.log('202:', req.body.user);
            req.session.loginUser = req.body.user;
        } else {
            req.session.flashMsg = '帳號或密碼錯誤';
            req.session.oriUser = req.body.user;
            req.session.oriPass = req.body.password;
        }
        res.redirect('/login');
    });


});
app.get('/logout',  (req, res)=>{

    delete req.session.loginUser;

    res.redirect('/login');
});

app.get('/try-moment', (req, res)=> {
    const myFormat = 'YYYY-MM-DD HH:mm:ss';
    const d = moment(new Date());

    res.contentType('text/plain');
    res.write(d.format(myFormat) + "\n");
    res.write(d.tz('Europe/London').format(myFormat) + "\n");

    res.end('');
});

app.use('/sales3', (req, res, next)=>{
    if(req.session.loginUser){
        next();
    } else {
        res.redirect('/login');
    }
});

app.get('/sales3', (req, res)=> {
    let sql = "SELECT * FROM `sales` ORDER BY `sid` DESC";
    db.query(sql, function(error, results, fields){
        for(let v of results){
            v.birthday2 = moment(v.birthday).format('YYYY-MM-DD');
        }
        //res.json(results);
        var listInfo = '';
        if(req.session.listInfo){
            listInfo = req.session.listInfo;
            delete req.session.listInfo;
        }

        res.render('sales-list', {
            rows: results,
            title: '資料列表',
            listInfo: listInfo,
        });
    });
});

app.get('/sales3/add', (req, res)=> {
    res.render('sales-add');
});
app.post('/sales3/add', (req, res)=> {
    var data = {
        success: false,
        type: 'danger',
        info: '',
        body: req.body, // 把傳入的資料再回應回去 echo
    };

    //req.body.sales_id
    let sql = "INSERT INTO `sales` (`sales_id`, `name`, `birthday`) VALUES (?, ?, ?)";
    db.query(sql,
        [req.body.sales_id, req.body.name, req.body.birthday],
        function(error, results){
            if(error){
                //res.json(error);
                data.info = '資料新增發生錯誤';
            } else {
                if(results.affectedRows===1){
                    data.success = true;
                    data.type = 'success';
                    data.info = '資料新增成功';
                }
            }
            res.render('sales-add', data);
        });
});

app.get('/sales3/add-ajax', (req, res)=> {
    res.render('sales-add-ajax');
});
app.post('/sales3/add-ajax', (req, res)=> {
    var data = {
        success: false,
        type: 'danger',
        info: '',
        code: 401,
        body: req.body, // 把傳入的資料再回應回去 echo
    };

    let sql = "INSERT INTO `sales` (`sales_id`, `name`, `birthday`) VALUES (?, ?, ?)";
    db.query(sql,
        [req.body.sales_id, req.body.name, req.body.birthday],
        function(error, results){
            if(error){
                //res.json(error);
                data.info = '資料新增發生錯誤';
            } else {
                if(results.affectedRows===1){
                    data.success = true;
                    data.type = 'success';
                    data.info = '資料新增成功';
                }
            }
            res.json(data);
        });
});

app.get('/sales3/remove/:sid([0-9]+)', (req, res)=> {
    // res.send(req.params.sid + ' ' + req.header('Referer') );

    var sid = parseInt(req.params.sid);
    if(isNaN(sid)){
        // ....
    } else {
        let sql = "DELETE FROM `sales` WHERE `sid`=? ";
        db.query(sql, [sid], (error, results)=>{
            if(results.affectedRows===1){
                req.session.listInfo = {
                    type: 'success',
                    text: `成功刪除編號 ${sid} 資料`,
                };
            } else {
                req.session.listInfo = {
                    type: 'danger',
                    text: `編號 ${sid} 資料沒有刪除`,
                };
            }

            res.redirect('/sales3');
            //res.json(results);
            // affectedRows

        });
    }

    // res.end();
    // res.send();
    // res.render();


});
app.get('/sales3/edit/:sid([0-9]+)', (req, res)=> {

    let sql = "SELECT * FROM `sales` WHERE sid=?";

    db.query(sql, [req.params.sid], (error, results)=>{
        //res.json(results);
        if(results && results[0]) {
            results[0].birthday = moment(results[0].birthday).format('YYYY-MM-DD');

            res.render('sales-edit', {
                row: results[0]
            });
        } else {
            res.redirect('/sales3');
        }
    });
});

app.post('/sales3/edit/:sid', (req, res)=> {
    var data = {
        success: false,
        type: 'danger',
        info: '',
        row: req.body, // 把傳入的資料再回應回去 echo
    };

    var sid = parseInt(req.params.sid);
    if(isNaN(sid)){
        // ....
        res.redirect('/sales3');
    } else {
        let sql = "UPDATE `sales` SET `sales_id`=?, `name`=?, `birthday`=? WHERE sid=?";

        db.query(sql, [
            req.body.sales_id,
            req.body.name,
            req.body.birthday,
            sid
        ], (error, results)=>{

            if(error){
                //res.json(error);
                data.info = '資料修改發生錯誤';
            } else {
                if(results.affectedRows===1){
                    data.success = true;
                    data.type = 'success';
                    data.info = '資料修改成功';
                }
            }
            res.render('sales-edit', data);
        });

    }

});

app.get('/try-bluebird', (req, res)=> {
    const output = [];
    db.queryAsync('SELECT * FROM `sales` WHERE sid=19')
        .then(results=>{
            output.push(results[0]);
            return db.queryAsync('SELECT * FROM `sales` WHERE sid=3');
        })
        .then(results=>{
            output.push(results[0]);
            res.json(output);
        });
});

app.use((req, res)=>{
    res.type('text/html');
    res.status(404);
    res.send('<h2> 找不到頁面 </h2>');
});

app.listen(3000, ()=>{
    console.log('server running~');
});








