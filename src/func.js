'use strict';

var func = function(a) {
    return a*a;
};

var func2 = function() {
    var r = 0;

    for(var i=1; i<=10; i++){
      r += i;
    }
    return r;
};

console.log(func(8));
console.log(func2());


